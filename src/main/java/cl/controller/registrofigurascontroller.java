/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.evaluaciond2.entity.Registrodefiguras;
import cl.registrodefiguras.dao.RegistrodefigurasJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Gera
 */
@WebServlet(name = "registrofigurascontroller", urlPatterns = {"/registrofigurascontroller"})
public class registrofigurascontroller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet registrofigurascontroller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet registrofigurascontroller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.getRequestDispatcher("registrodefigura.jsp").forward(request, response);
                
          
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        String nombre=request.getParameter("nombre");
        String serie=request.getParameter("serie");
        String linea=request.getParameter("linea");
        String lanzamiento=request.getParameter("lanzamiento");
        String piezas=request.getParameter("piezas");
        
        System.out.println("nombre:" + nombre);
        System.out.println("serie:" + serie);
        System.out.println("linea:" + linea);
        System.out.println("lanzamiento:" + lanzamiento);
        System.out.println("piezas:" + piezas);
        
        Registrodefiguras registrodefiguras=new Registrodefiguras();
        registrodefiguras.setNombre(nombre);
        registrodefiguras.setSerie(serie);
        registrodefiguras.setLinea(linea);
        registrodefiguras.setLanzamiento(lanzamiento);
        registrodefiguras.setCantidaddepiezas(piezas);
         
     
        RegistrodefigurasJpaController dao= new RegistrodefigurasJpaController();
        
        try {
            dao.create(registrodefiguras);
        } catch (Exception ex) {
            Logger.getLogger(registrofigurascontroller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        List<Registrodefiguras> figura = dao.findRegistrodefigurasEntities();
        request.setAttribute("listafiguras", figura);
        request.getRequestDispatcher("registrodefigura.jsp").forward(request, response);// aca puedo linkear al mantenedor para que se vea en la lista
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

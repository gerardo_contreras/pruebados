/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.evaluaciond2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gera
 */
@Entity
@Table(name = "registrodefiguras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registrodefiguras.findAll", query = "SELECT r FROM Registrodefiguras r"),
    @NamedQuery(name = "Registrodefiguras.findByNombre", query = "SELECT r FROM Registrodefiguras r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Registrodefiguras.findBySerie", query = "SELECT r FROM Registrodefiguras r WHERE r.serie = :serie"),
    @NamedQuery(name = "Registrodefiguras.findByLinea", query = "SELECT r FROM Registrodefiguras r WHERE r.linea = :linea"),
    @NamedQuery(name = "Registrodefiguras.findByLanzamiento", query = "SELECT r FROM Registrodefiguras r WHERE r.lanzamiento = :lanzamiento"),
    @NamedQuery(name = "Registrodefiguras.findByCantidaddepiezas", query = "SELECT r FROM Registrodefiguras r WHERE r.cantidaddepiezas = :cantidaddepiezas")})
public class Registrodefiguras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 50)
    @Column(name = "serie")
    private String serie;
    @Size(max = 50)
    @Column(name = "linea")
    private String linea;
    @Size(max = 10)
    @Column(name = "lanzamiento")
    private String lanzamiento;
    @Size(max = 5)
    @Column(name = "cantidaddepiezas")
    private String cantidaddepiezas;

    public Registrodefiguras() {
    }

    public Registrodefiguras(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getLanzamiento() {
        return lanzamiento;
    }

    public void setLanzamiento(String lanzamiento) {
        this.lanzamiento = lanzamiento;
    }

    public String getCantidaddepiezas() {
        return cantidaddepiezas;
    }

    public void setCantidaddepiezas(String cantidaddepiezas) {
        this.cantidaddepiezas = cantidaddepiezas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registrodefiguras)) {
            return false;
        }
        Registrodefiguras other = (Registrodefiguras) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.evaluaciond2.entity.Registrodefiguras[ nombre=" + nombre + " ]";
    }
    
}

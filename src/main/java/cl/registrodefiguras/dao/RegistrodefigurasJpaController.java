/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.registrodefiguras.dao;

import cl.evaluaciond2.entity.Registrodefiguras;
import cl.registrodefiguras.dao.exceptions.NonexistentEntityException;
import cl.registrodefiguras.dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Gera
 */
public class RegistrodefigurasJpaController implements Serializable {

    public RegistrodefigurasJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_evaluacion2");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registrodefiguras registrodefiguras) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registrodefiguras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistrodefiguras(registrodefiguras.getNombre()) != null) {
                throw new PreexistingEntityException("Registrodefiguras " + registrodefiguras + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registrodefiguras registrodefiguras) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registrodefiguras = em.merge(registrodefiguras);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = registrodefiguras.getNombre();
                if (findRegistrodefiguras(id) == null) {
                    throw new NonexistentEntityException("The registrodefiguras with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registrodefiguras registrodefiguras;
            try {
                registrodefiguras = em.getReference(Registrodefiguras.class, id);
                registrodefiguras.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registrodefiguras with id " + id + " no longer exists.", enfe);
            }
            em.remove(registrodefiguras);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registrodefiguras> findRegistrodefigurasEntities() {
        return findRegistrodefigurasEntities(true, -1, -1);
    }

    public List<Registrodefiguras> findRegistrodefigurasEntities(int maxResults, int firstResult) {
        return findRegistrodefigurasEntities(false, maxResults, firstResult);
    }

    private List<Registrodefiguras> findRegistrodefigurasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registrodefiguras.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registrodefiguras findRegistrodefiguras(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registrodefiguras.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistrodefigurasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registrodefiguras> rt = cq.from(Registrodefiguras.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

<%-- 
    Document   : registrodefigura
    Created on : Jul 11, 2021, 7:28:22 PM
    Author     : Gera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
     <form  name="form" action="registrofigurascontroller" method="POST">
        <h1>Agregar una figura</h1>
        
        <label for="nombre">Nombre: </label>

        <input type="text" name="nombre" id="nombre">

        <br/> <br/>

        <label for="serie">Serie: </label>

        <input type="text" name="serie" id="serie">

        <br/> <br/>

        <label for="linea">Linea: </label>

        <input type="text" name="linea" id="linea">

        <br/> <br/>
        
        <label for="lanzamiento">Año de lanzamiento: </label>

        <input type="text" name="lanzamiento" id="lanzamiento">

        <br/> <br/>
        
        <label for="piezas">Cantidad de piezas: </label>

        <input type="text" name="piezas" id="piezas">
        <br/> <br/>
        <br/> <br/>
  
        <button type="submit" class="btn btn-success">Enviar</button>

    </form>
    </body>
</html>
